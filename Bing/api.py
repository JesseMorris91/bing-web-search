from pprint import pprint
import requests
from decouple import config

#NOT for production - API dict keys for reference

subscription_key = config('subscription_key', default='')

search_url = "https://api.bing.microsoft.com/v7.0/search"
search_term = 'iphone 13'

headers = {"Ocp-Apim-Subscription-Key": subscription_key}
params = {"q": search_term, "textDecorations": True, "textFormat": "HTML"}
response = requests.get(search_url, headers=headers, params=params)
response.raise_for_status()
search_results = response.json()

# title = search_results['deeplinks'['name']]

#dict_keys(['_type', 'queryContext', 'webPages', 'news', 'relatedSearches', 'videos', 'rankingResponse'])
#webPages - dict_keys(['webSearchUrl', 'totalEstimatedMatches', 'value'])
#queryContext - dict_keys([originalQuery])
#videos - dict_keys(['id', 'readLink', 'webSearchUrl', 'isFamilyFriendly', 'value', 'scenario'])
#news - dict_keys(['id', 'readLink', 'value'])

# python .\Bing\api.py / run command to print

# pages = search_results['webPages']
# results = pages['value'][0]
# items = results['items']
# description = results['description']

videos = search_results['videos']
value = videos['value'][0]['contentUrl']

pprint(value)
#description, publisher, thumbnail, embedHtml, creator,

# pages = search_results['webPages']
# results = pages['value'][0]['richFacts'].keys()
# results = pages['value'][0].keys()
# results = pages['value']


# pprint(results)

# for i in (results):
#   name = i['name']
#   snippet = i['snippet']
#   url = i['url']
# print(name)

# pprint(results)
# pprint(pages)
# pprint(results[0])
# print(search_results.keys())
# pprint(pages.keys())

# print(results[0]['name'])
# print(results[0]['snippet'])
# print(results[0]['url'])

# print(results[1]['name'])
