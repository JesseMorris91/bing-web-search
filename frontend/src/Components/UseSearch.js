import { useState, useEffect } from 'react';
import axios from 'axios';
import NavBar from './Navbar/Nav';
import SearchResults from './SearchResults';

const BASE_URL = 'https://api.bing.microsoft.com/v7.0/search';
const API_KEY = '0760f20a58f340a4ba3ce10857c43142'

export const useSearch = ( { searchTerm } ) => {
    const [data, setData] = useState([]);
    // const { searchData } = SearchResults({ searchResultData: data })

    useEffect(() => {
        const fetchData = async () => {
            if (!searchTerm) return;
            try {
                const response = await axios.get(`${BASE_URL}`, {
                    headers: {
                        'Ocp-Apim-Subscription-Key': API_KEY,
                    },
                    params: {
                        'q': `${searchTerm}`,
                        'textDecorations': true,
                        'textFormat': 'HTML',
                        'count': 10,
                    },
                });
                setData(response.data.webPages.value);
                console.log(response.data.webPages.value);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };
        fetchData();
    }, [searchTerm]);

    return data;

};

export default useSearch;
