import React, { useState } from 'react';
import './NavBar.css';
import logo_light from '../../assets/spigot-logo.png';
import logo_dark from '../../assets/logo_light2.png';
import search_icon_light from '../../assets/search-w.png';
import search_icon_dark from '../../assets/search-b.png';
import toggle_light from '../../assets/night.png';
import toggle_dark from '../../assets/day.png';
import useSearch from '../UseSearch';
// import { debounce } from 'lodash';


function NavBar({theme, setTheme, setSearchQuery}) {
  // const [searchQuery, setSearchQuery] = useState('');
  // const data = useSearch({ searchTerm: searchQuery});
  /*console log showing data input typed into search bar*/
  // console.log(searchQuery);

  /* ternary clause to change theme between light and dark mode */
  const toggle_mode = () => {
    theme == 'light' ? setTheme('dark') : setTheme('light');
  }

  // async function handleChange(event) {
  //   setSearchQuery(await text(event.target.setSearchQuery));
  // }

  // const handleSubmit = (event) => {
  //   event.preventDefault()
  //   console.log(event.target.elements.value)
  // }



  // const handleSearch = (event) => {
  //   setSearchTerm(event.target.value);
  // };

  return (
    <div className='navbar' >
        <img src={theme == 'light' ? logo_light : logo_dark}
        alt=''
        className='logo' />
        <ul className='nav-list'>
          <li>SEARCH</li>
        </ul>
      <div className='search-box'>
        <form>
          <input
          className='search-bar'
          type='text'
          placeholder='Search'
          onChange={(e) => setSearchQuery(e.target.value)}
          ></input>
        </form>
        <img className='search-icon'
        src={theme == 'light' ? search_icon_light : search_icon_dark}
        alt=''
        />
      </div>
      <img onClick={() => {toggle_mode()}}
      src={theme == 'light' ? toggle_light : toggle_dark}
      alt='' className='toggle-icon'
      ></img>
    </div>
  )
}

// onChange={(e) => setSearchQuery(e.target.value)

export default NavBar
