import React from 'react'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

// In development

export default function MultiSlider(image, title, description, website) {

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
  };

  return (
    <div className="slider-container">
      <Slider {...settings}>

          <div>
            <img id="hide-text" src={image} alt={title} />
            <a id="title-decorator" href="title">{title}</a>
            <p id="description-text">{description}</p>
            <a id="url-decorator" href="{website}">{website}</a>
          </div>
          <div>
            <h3>2</h3>
          </div>
          <div>
            <h3>3</h3>
          </div>
          <div>
            <h3>4</h3>
          </div>
          <div>
            <h3>5</h3>
          </div>
          <div>
            <h3>6</h3>
          </div>

      </Slider>
    </div>
  )
};
