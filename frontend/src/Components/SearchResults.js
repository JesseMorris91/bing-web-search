import React, { useState, useEffect } from 'react';
import axios from 'axios';
import TileItems from './TileItems';
import VideoItems from './video';
import NavBar from './Navbar/Nav';
import Nav from '../Components/Navbar/Nav';
// import useSearch from './UseSearch';
// import useSearch from './UseSearch';
import useSearch from './UseSearch';



const SearchResults = ({searchResults}) => {

    const [theme, setTheme] = useState('light');
    // const [data, setData] = useState(null);
    const [video, setVideo] = useState(null);
    // const [data, setData] = useState('');
    // const myData = useSearch({searchTerm: data});

    // const [data, setData] = useState('');

    // const [myData, setMyData] = useState('');
    // const data = useSearch({searchTerm: myData});





    // function handleSearchData(searchData) {
    //   setSearchData(searchData);
    // }

  // Currently in development
    // useEffect(() => {
    //     const fetchVideo = async () => {
    //         try {
    //             const response = await axios.get('http://localhost:8000/video/');
    //             // setAd(response.video)
    //             setVideo(response.video);
    //         } catch (error) {
    //             console.error('Error fetching video:', error);
    //         }
    //     };
    //     fetchVideo();
    // }, []);


    // fetching the HttpResponse Object from Django views.py file
    // useEffect(() => {
    //     const fetchData = async () => {
    //         try {
    //             const response = await axios.get('http://localhost:8000/home/');
    //             setAd(response.data);
    //             setData(response.data);
    //             console.log(data);
    //         } catch (error) {
    //             console.error('Error fetching data:', error);
    //         }
    //     };
    //     fetchData();
    // }, []);

    return (
        <div className=''>
          <main className={`main-container ${theme}`}>
            <section className="eighty" id="item-tiles">
              <h1>Search Results</h1>
              {searchResults ? (
                  <ul>
                    {searchResults.map((i) => (
                      <li>
                        <TileItems
                            key={i.id}
                            image={i.thumbnailUrl}
                            title={i.name}
                            description={i.snippet}
                            website={i.url}
                        />
                        </li>
                    ))}
                  </ul>
                  ) : (
                    <p>Loading...</p>
                  )}
            </section>
            <section id="item-tiles-right" className="twenty">
              <h2 id='col-2-header'>News</h2>
                 {video ? (
                    <ul>
                      {/* this section is still under development */}
                      {video.map((v) => (
                          <iframe
                            id='ytplayer'
                            src={v.contentUrl}
                            frameborder='0'
                            allow='autoplay; encrypted-media'
                            width='100%'
                            height='360'
                            title='video'
                          />
                       ))}
                    </ul>
                    ) : (
                      <p>Loading...</p>
                    )}
            </section>
          </main>
        </div>
    );
};

export default SearchResults;
