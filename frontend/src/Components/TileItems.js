// Used Object destructuring instead of props for Search Results
export default function TileItems({image, title, description, website}) {


  return (
    <li>
      <img id="hide-text" src={image} alt='' />
      <a id="title-decorator" href={website} >{title}</a>
      <p id="description-text">{description}</p>
      <a id="url-decorator" href={website}>{website}</a>
    </li>
  );
}
