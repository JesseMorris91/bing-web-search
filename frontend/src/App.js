import logo from './logo.svg';
import './App.css';
import React, { useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import listWebSearch from './listWebSearch';
import SearchResults from './Components/SearchResults';
import Nav from './Components/Navbar/Nav';
import useSearch from './Components/UseSearch';



function App() {

  const [theme, setTheme] = useState('light');
  const [searchQuery, setSearchQuery] = useState('');
  const searchData = useSearch({ searchTerm: searchQuery })

  return (
// div is the small space right below header
    <BrowserRouter>
      <div className={`background-toggle ${theme}`}>
        <Nav
          theme={theme}
          setTheme={setTheme}
          setSearchQuery={setSearchQuery}/>

          <Routes>

            <Route
              path="/"
              element={<SearchResults searchResults={searchData}/>}
            />
          </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
