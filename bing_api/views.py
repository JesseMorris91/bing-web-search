from django.shortcuts import render
from pprint import pprint
import requests
# from decouple import config
from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from pathlib import Path
from django.conf import settings
import os
from bing_api import creds


#Bing web search api - search terms
def home(request):
  url = 'https://api.bing.microsoft.com/v7.0/search'
  search_url = "https://api.bing.microsoft.com/v7.0/search"
  search_term = 'iphone 13'

  headers = {"Ocp-Apim-Subscription-Key": creds.API_KEY}
  params = {"q": search_term, "textDecorations": True, "textFormat": "HTML"}
  response = requests.get(search_url, headers=headers, params=params)
  response.raise_for_status()
  search_results = response.json()
  data = response.json()
  pprint(data)

  #below is to view in React
  pages = search_results['webPages']
  results = pages['value']

  return JsonResponse(results, safe=False)

#Bing web api - videos
def video(request):
  url = 'https://api.bing.microsoft.com/v7.0/search'
  search_url = "https://api.bing.microsoft.com/v7.0/search"
  search_term = 'iphone 13'

  headers = {"Ocp-Apim-Subscription-Key": creds.API_KEY}
  params = {"q": search_term, "textDecorations": True, "textFormat": "HTML"}
  response = requests.get(search_url, headers=headers, params=params)
  response.raise_for_status()
  search_results = response.json()
  data = response.json()
  pprint(data)

  #below is to view in React
  pages = search_results['videos']
  results = pages['value'][0]
  #Axios fetch requires HttpResponse Object
  return JsonResponse(results, safe=False)


  # below is to view in Django Templates
  # pages = data['webPages']
  # search_results = pages['value']

  # context = {
  #   'search_results': search_results
  # }

  # return render(request, 'bing_api/home.html', context)
