from django.apps import AppConfig


class BingApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bing_api'
