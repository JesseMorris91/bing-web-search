from django.urls import path
from . import views

urlpatterns = [
    path('home/', views.home, name='home'),
    path('video/', views.video, name='video'),
    # path('search/', views.search_api, name='search')
]
